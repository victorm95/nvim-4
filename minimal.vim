" This is a minimal vim or neovim configuration
" It's designed to be useful with not dependencies

" =============================================
" Basic configuration
" =============================================

" Disable backwards compatibility with vi
set nocompatible

" {{{ Syntax highlight
syntax on
set termguicolors
colorscheme default
" Enable opacity
hi Normal guibg=none

" Enable filetype plugins and indent
filetype plugin indent on
" }}}

" {{{ Files
" Set the new line character to \n instead of \r\n
set fileformat=unix
set fileencoding=utf-8
" }}}

" {{{ Buffers
" Don't require save to change buffer
set hidden
" }}}

" {{{ Line numbers
set relativenumber
set number
" }}}

" {{{ Mouse support
set mouse=a
" }}}

" {{{ Cursor
" Use the same cursor of NORMAL mode on INSERT mode
" set guicursor=
" }}}


" {{{ Indentation
set autoindent
" Width of tab
set tabstop=4

" Soft tab. A.K.A space instead of tab
set shiftwidth=2
set softtabstop=2
set expandtab
" }}}

" {{{ Backspace
" Continue deleting on the previous line
set backspace=indent,eol,start
" }}}

" {{{ Scroll
" Start scrolling when it's 8 lines away
set scrolloff=8
" }}}

" {{{ Guides
" Put a line on 80 characters to know when is too much code on a line
set colorcolumn=80

" Character to show new line, trailing spaces and tabs
set list listchars=eol:¬,tab:››,trail:·,extends:»,precedes:«
" }}}

" {{{ Wrap lines
" Disable line wrapping
set nowrap
" }}}


" =============================================
" Custom key bindings
" =============================================

let mapleader = "\<Space>"

" {{{ Windows
" New horizontal window
nnoremap <silent> <Leader>wv :vsplit<CR>

" New vertial window
nnoremap <silent> <Leader>ws :split<CR>

" Focus left window
nnoremap <silent> <Leader>wh <C-w>h
nnoremap <silent> <C-h> <C-w>h

" Focus bottom window
nnoremap <silent> <Leader>wj <C-w>j
nnoremap <silent> <C-j> <C-w>j

" Focus top window
nnoremap <silent> <Leader>wk <C-w>k
nnoremap <silent> <C-k> <C-w>k

" Focus right window
nnoremap <silent> <Leader>wl <C-w>l
nnoremap <silent> <C-l> <C-w>l

" Move window to left
nnoremap <silent> <Leader>wmh <C-w>H

" Move window to bottom
nnoremap <silent> <Leader>wmj <C-w>J

" Move window to up
nnoremap <silent> <Leader>wmk <C-w>K

" Move window to right
nnoremap <silent> <Leader>wml <C-w>L

" Close/Kill window
nnoremap <silent> <Leader>wc :q<CR>
nnoremap <silent> <Leader>wk :q<CR>

" Force kill window
nnoremap <silent> <Leader>wK :q!<CR>
" }}}

" {{{ Buffers
" Next buffer
nnoremap <silent> <Leader>bn :bnext<CR>
nnoremap <silent> <Leader>b] :bnext<CR>
nnoremap <silent> <Tab> :bnext<CR>

" Previous buffer
nnoremap <silent> <Leader>bp :bprev<CR>
nnoremap <silent> <Leader>b[ :bprev<CR>
nnoremap <silent> <S-Tab> :bprev<CR>

" Buffer list
nnoremap <silent> <Leader>bl :ls<CR>

" Save buffer
nnoremap <silent> <Leader>bs :w<CR>

" Kill buffer
nnoremap <silent> <Leader>bk :bdelete<CR>

" Force kill buffer
nnoremap <silent> <Leader>bK :bdelete!<CR>
" }}}

" {{{ Move code
" Indent selected code
vnoremap <silent> < <gv
vnoremap <silent> > >gv

" Move selected code up and down
vnoremap <silent> J :m '>+1<CR>gv=gv
vnoremap <silent> K :m '<-2<CR>gv=gv
" }}}

" {{{ Search
" Clear seach highlight
nnoremap <silent> <Leader><Esc> :noh<CR>
" }}}


" =============================================
" Autocommands. Like hooks
" =============================================

function! TrimTrailingSpaces()
  let l:save = winsaveview()
  keeppatterns %s/\s\+$//e
  call winrestview(l:save)
endfunction

augroup HOOKS
  autocmd!
  autocmd BufWritePre * :call TrimTrailingSpaces()
augroup END
