" ======================================
" Config
" ======================================

" Color scheme
set termguicolors
colorscheme onedark
" Opacity
hi Normal ctermbg=none guibg=none
hi LineNr ctermbg=none guibg=none

" Disable backwards compatibility with vi
" set nocompatible

" Line numbers
set relativenumber
set number

" Enable syntax highlight
" syntax on

" Enable filetpe plugins and indent
" filetype plugin indent on

" Indentation
set autoindent
set tabstop=4
set softtabstop=2
set shiftwidth=2
set expandtab

" Enable mouse
" set   mouse=a

" File settings
set fileformat=unix
set fileencoding=utf-8

" Line wrap
set nowrap

" Custom characters for end of line, trailing spaces, tabs, etc
set list listchars=eol:¬,tab:››,trail:·,extends:»,precedes:«

" Clipboard
set clipboard+=unnamedplus

" Search
" Don't show hightlight
set nohlsearch
" Incremental search: Highlight while typing
set incsearch
" set ignorecase
" set smartcase

" Alway use block cursor
set guicursor=

" Always has an extra column on left
set signcolumn=yes

" Draw a line at 80 characters
set colorcolumn=80

" Start to scroll when is 10 lines away
set scrolloff=10

" Don't require save to change buffer
set hidden

set noerrorbells
