call plug#begin('~/.config/nvim/plugged')
source ~/.config/nvim/packages.vim
call plug#end()

source ~/.config/nvim/load.vim
source ~/.config/nvim/keymappings.vim
source ~/.config/nvim/config.vim
