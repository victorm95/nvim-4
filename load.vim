" =========
" NERD Tree
" =========
let g:NERDTreeIgnore = ['node_modules', 'vendor', 'dist']
let g:NERDTreeDirArrowExpandable = '▸'
let g:NERDTreeDirArrowCollapsible = '▾'


" =====
" CtrlP
" =====
let g:ctrlp_map = '<C-p>'
let g:ctrlp_cdm = 'CtrlP'
let g:ctrlp_user_command = ['.git', 'cd %s && git ls-files -co --exclude-standard']


" ====
" JSON
" ====
let g:vim_json_syntax_conceal = 0

" ===
" ALE
" ===
let g:ale_completion_enabled = 1
let g:ale_completion_autoimport = 1
let g:ale_fix_on_save = 1
let g:ale_fixers = {
      \'*': ['remove_trailing_lines', 'trim_whitespace'],
      \'javascript': ['prettier'],
      \'typescript': ['prettier'],
      \'typescriptreact': ['prettier'],
      \}


" ========
" Deoplete
" ========
" let g:deoplete#enable_at_startup = 1
" call deoplete#custom#option('sources', {
" \ '_': ['ale'],
" \})


" =======
" Airline
" =======
let g:airline#extensions#tabline#enabled = 1

" ========
" One Dark
" ========
let g:onedark_terminal_italics = 1
